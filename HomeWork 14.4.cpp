﻿
#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter text: ";
    std::string EnterText;
    std::getline(std::cin, EnterText);
    std::cout << "String 'EnterText'\n" << "Text ";
    std::cout << "'" << EnterText << "'\n";
    std::cout << "Dlina stroki - ";
    std::cout << EnterText.length() << " \n";
    std::cout << "First simvol - " << EnterText.front() << '\n';
    std::cout << "Last simvol - " << EnterText.back() << "\n";
    return 0;
}

